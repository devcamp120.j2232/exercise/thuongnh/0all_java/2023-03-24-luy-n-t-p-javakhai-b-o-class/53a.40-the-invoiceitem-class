import models.*;

public class App {
    public static void main(String[] args) throws Exception {

        InvoiceItem invoiceItem1 = new InvoiceItem("ttuong09", "pizza hai san", 6, 60.0);
        InvoiceItem invoiceItem2 = new InvoiceItem("linh070", "bun bo ", 1, 25.0);
        print("invoiceItem1");
        print(invoiceItem1.getId());
        print(invoiceItem1.getDesc());
        print(invoiceItem1.getQty());
        print(invoiceItem1.getUnitPrice());
        print(invoiceItem1.getTotal());
        print(invoiceItem1.toString());



        print("invoiceItem2");
        print(invoiceItem2.getId());
        print(invoiceItem2.getDesc());
        print(invoiceItem2.getQty());
        print(invoiceItem2.getUnitPrice());
        print(invoiceItem2.getTotal());
        print(invoiceItem2.toString());


        print("tổng giá trị của hai đối tượng trên là");
        print(invoiceItem1.getTotal() + invoiceItem2.getTotal());

    }


    public static void print(Object paramA){
        System.out.println(paramA);
    }
    


}


